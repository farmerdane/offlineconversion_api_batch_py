
# Offline Conversion API Integration

## Requirements
- Python 3
- Facebook Business SDK for Python v12+

## Facebook Business SDK for Python - Library Dependencies

The easiest way to install the SDK is via ``pip`` in your shell.

**NOTE**: For Python 3, use ``pip3`` and ``python3`` instead.

**NOTE**: Use ``sudo`` if any of these complain about permissions. (This might
happen if you are using a system installed Python.)

If you don't have pip:

```
easy_install pip
```

Now execute when you have pip:

```
pip install facebook_business
```

https://github.com/facebook/facebook-python-business-sdk

## Config File
A `config.py` file needs to be created as it contains the settings for the access tokens and pixel IDs.

```
BATCH_SIZE=1000
ACCESS_TOKEN_SU="replace_with_access token"
OFFLINE_EVENT_SET="replace_with_offline_eventset_id"
```