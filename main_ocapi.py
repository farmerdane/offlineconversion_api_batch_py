import datetime
import time
import config
import json

from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.offlineconversiondataset import OfflineConversionDataSet
from facebook_business.adobjects.serverside.normalize import Normalize


class FacebookService:
    def __init__(self, access_token, offline_event_set_id):
        self.api = FacebookAdsApi.init(access_token=access_token, debug=True)
        self.offline_event_set_id = offline_event_set_id
        self.offline_dataset = OfflineConversionDataSet(offline_event_set_id)

    def chunks(self, l, n):
        n = max(1, n)
        return (l[i:i + n] for i in range(0, len(l), n))

    def sendBatchedData(self, data, batchsize=1000):
        events = []
        for row in data:
            try:
                payload = {}

                # normalization: https://github.com/facebook/facebook-python-business-sdk/blob/fbc3b6a5c2d54bd4184163b290dd614b668e4ee0/facebook_business/adobjects/serverside/normalize.py
                match_keys = {
                    "email": Normalize.normalize(field="em", data=row["em"], hash_field=True),
                    "phone": Normalize.normalize(field="ph", data=row["ph"], hash_field=True),
                    "fn": Normalize.normalize(field="fn", data=row["fn"], hash_field=True),
                    "ln": Normalize.normalize(field="ln", data=row["ln"], hash_field=True),
                }

                payload["order_id"] = row["order_id"];
                payload["currency"] = row["currency"];
                payload["value"] = row["value"];
                payload["event_name"] = row["event_name"];
                payload["event_time"] = row["event_time"];

                custom_data = {
                    "event_source": row["event_source"]
                }

                payload["match_keys"] = match_keys;
                payload["custom_data"] = custom_data;

                # transform dict into JSON string
                events.append(json.dumps(payload))
            except TypeError as e:
                if hasattr(e, 'message'):
                    print(e.message)
                else:
                    print(e)

        # Offline Conversion API cannot take more than 2000 events at once
        # Break it into request chunks if it exceeds that amount
        event_chunks = self.chunks(events, batchsize)
        for event_chunk in event_chunks:
            params = {
                'upload_tag': 'upload_test_' + datetime.datetime.now().replace(microsecond=0).isoformat(),
                'data': event_chunk,
            }
            print(params)
            ret = self.offline_dataset.create_event(params=params)
            print(ret)


class TransformQuery:
    def transformQueryData(self, data):
        if not data:
            return []

        for row in data:
            row["event_name"] = "Purchase"
        return data

if __name__ == '__main__':
    fs = FacebookService(
        access_token=config.ACCESS_TOKEN_SU,
        offline_event_set_id=config.OFFLINE_EVENT_SET
    )
    tqd = TransformQuery()

    # Query DB here and return array of the following keys/data format
    sampleData = [
        {
            "order_id": "123-234-abcd",
            "ln": "Farmer",
            "fn": "Fane",
            "em": "FarmerDane@yahoo.com",
            "ph": "+4407530000000",
            "currency": "GBP",
            "value": "50.25",
            "event_source": "in_store",
            "event_time": int(time.time())
        },
        {
            "order_id": "456-789-defg",
            "ln": "Smith",
            "fn": "Joe",
            "em": "js@yahoo.com",
            "ph": "+4407531111111",
            "currency": "GBP",
            "value": "80",
            "event_source": "in_store",
            "event_time": int(time.time())
        }
    ]

    # enrich and/or transform any values coming from the DB for Conversion API compatibility
    sampleData = tqd.transformQueryData(sampleData)
    fs.sendBatchedData(sampleData, config.BATCH_SIZE)
